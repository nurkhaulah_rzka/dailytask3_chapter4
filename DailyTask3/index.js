const express = require("express");
const app = express();

const port = 3000;
const allData = require("./data/data");
const { filterData } = require("./data/filterData");

app.set("view engine", "ejs"); // menggunakan view engine ejs
app.use(express.static("public")); //built in middleware

// Halaman Home
app.get("/", (req, res) => {
  res.render("index", {
    name: "Nur Khaulah Arrizka",
    title: "Halaman Home",
  });
});

// Halaman About
app.get("/about", (req, res) => {
  res.render("about", {
    title: "Halaman About",
  });
});

// req.query
app.get("/dataPers", (req, res) => {
  let data = filterData(allData, req.query.favoriteFruit);
  // console.log(data)
  res.render("dataPers", {
    title: "Halaman Data",
    data: data,
  });
});

// Error Handler
app.use("/", (req, res) => {
  res.status(404);
  res.render("404");
});

// server
app.listen(port, () => {
  console.log(`Example app listening on http://localhost:${port}`);
});
